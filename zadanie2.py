# Rozwiń zadnie 1, aby posiadało określone funkcje:
# ===============CENZOR==================

# Dodaj słowo na liste słów zakazanych
# Wyświetl słowa zakazane
# Ocenzuruj tekst

bad_words = [] #stworzenie listy globalnej

def add_bad_words(): #stworzenie funkcji dodajacej brzydkie slowa do listy globalnej
    global bad_words #import listy globalnej
    x = str(input("Podaj brzydkie slowa: ")) #zmienna x zbierajaca brzydkie slowa od użytkownika
    bad_words.append(x) #dołączenie brzydkich slow do listy globalnej
    menu() #powrot do menu

def show_bad_words(): #stworzenie funkcji pokaż brzydkie słowa
    global bad_words #import listy globalnej
    print(bad_words) #pokaż brzydkie slowa
    menu() #powrót do menu

def censor(): #stworzenie funkcji cenzurującej
    global bad_words #import listy globalnej
    text = str(input("Podaj tekst do ocenzurowania: ")) #zbieranie od użytkownika tekstu do ocenzuorwania i przypisanie do zmiennej text
    splitted_text = text.split(" ") #rozdzielenie textu po spacjach i stworzenie listy
    output = "" #stworzenie pustej zmiennej string
    for word in splitted_text: #pętla for sprawdzająca jeśli w liście:
        if word in bad_words: #jeśli element z listy jest w brzydkich słowach:
            output += "**** " #dodaj do zmiennej output i ocenzuruj
        else: #pozostałe
            output += word + " " #dodaj do zmiennej output wraz ze spacją
    print(output) #pokaż zmienna output
    menu() #powrót do menu

def menu(): #stworzenie funkcji menu
    global bad_words #import listy globalnej
    print("""
===============CENZOR==================

1. Dodaj słowo na liste słów zakazanych
2. Wyświetl słowa zakazane
3. Ocenzuruj tekst\n""")
    x = int(input("Wybierz zadanie: ")) #pobranie od użytkownikia decyzji i przypisanie do zmiennej
    if x == 1: #jeśli wybór = 1 to:
        add_bad_words() #uruchom funkcje dodaj brzydkie słowa
    elif x == 2: #jeśli wybór = 2 to:
        show_bad_words() #uruchomn funkcje pokaż brzydkie słowa
    elif x == 3: #jeśli wybór = 3 to:
        censor() #uruchom funkcje cenzurującą
menu() #uruchom menu