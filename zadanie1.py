# Utwórz metodę o sygnaturze def censor(str, words).
#     Metoda:
#     rozbije łańcuch tekstowy na liste wyrazów za pomocą separatora spacji " ",
#     sprawdzi, czy nie znajdują się w nim słowa niedozwolone zawarte w listy words,
#     jeśli tak -- zamieni je na cztery gwiazdki (****)
#     ponownie połączy liste w string i zwróci wartość.

def censor(text, bad_words): #tworzenie funkcji która cenzuruje słowa
    text = str(text) #stworzenie zmiennej typu string
    #bad_words = list(bad_words) #przypisanie do typu listy
    splitted_text = text.split(" ") #rozdzielenie tekstu przez spacje i stworzenie listy
    output = "" #stworzenie pustej zmiennej string
    for word in splitted_text: #pętla for sprawdzająca jeśli w liście:
        if word in bad_words: #jeśli element z listy jest w brzydkich słowach:
            output += "**** " #dodaj do zmiennej output i ocenzuruj
        else: #pozostałe
            output += word + " " #dodaj do zmiennej output wraz ze spacją
    print(output) #wyświetl zmienną output
lista_brzydkich_slow = ["rzutnik", "kaloryfer", "laptop", "cocacola"] #stworzenie listy brzydkich słów
tekst = "rzutnik grzeje, kaloryfer też, a co dopiero laptop" #stworzenie zdania w zmiennej tekst
censor(tekst, lista_brzydkich_slow) #uruchomienie funkcji ze zmiennymi podanymi wyżej