plik = open("mojtekst.txt", "r") #tryb odczytu
for line in plik: #zawartości pliku linia po lini
    print(line.strip())
plik.close() #Plik należy zawsze zamknąć, żeby dane nie były blokowane

plik2 = open("mojtekst2.txt", "w") #tryb zapisu - tworzy plik jesli go nie ma i nadpisuje
plik2.write("xxx")
plik2.write("xxxx")
plik2.close()


plik3 = open("mojtekst3", "a") #tryb doczepiania do pliku (append) czyli dodaje tekst
plik3.write("yyyy")
plik3.write("aaa")
plik3.close()